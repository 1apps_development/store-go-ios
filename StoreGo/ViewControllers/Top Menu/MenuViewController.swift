//
//  MenuViewController.swift
//  StoreGo
//
//  Created by DREAMWORLD on 22/03/22.
//

import UIKit
import SOTabBar
//import Alamofire

class MenuCell : UICollectionViewCell
{
    
    @IBOutlet weak var cell_view: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_images: UIImageView!
    
    func configureCell(_ menu: MenuItem){
        img_images.image = UIImage.init(named: menu.image)
        lbl_title.text = menu.title
        if menu.isSelected{
            cell_view.backgroundColor = UIColor.init(named: "Green_Color")
            lbl_title.textColor = .white
            img_images.image = img_images.image?.withRenderingMode(.alwaysTemplate)
            img_images.tintColor = .white
        }else{
            cell_view.backgroundColor = .white
            lbl_title.textColor = UIColor.init(named: "App_Bg_Color")
            img_images.image = img_images.image?.withRenderingMode(.alwaysTemplate)
            img_images.tintColor = UIColor.init(named: "App_Bg_Color")
        }
    }
    
}
protocol MenuOptionSelectDelegate: NSObject{
    func menuOptionSelected(_ title: String)
}


class MenuViewController: UIViewController {
    
    @IBOutlet weak var menuCollection: UICollectionView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var mDelegate: MenuOptionSelectDelegate?
    
    var parentVC: UIViewController!
    
    var selectedItem: String!
    var arrMenuItems = [MenuItem(title: "Dashboard", image: "tab_dash_sel", isselected: true),
                        MenuItem(title: "Orders", image: "tab_orders", isselected: false),
                        MenuItem(title: "Products", image: "tab_products", isselected: false),
                        MenuItem(title: "Categories", image: "tab_cat", isselected: false),
                        MenuItem(title: "Statistics", image: "ic_Statistics", isselected: false),
                        MenuItem(title: "Plans", image: "ic_Plans", isselected: false),
                        MenuItem(title: "Settings", image: "tab_settings", isselected: false),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        for item in arrMenuItems{
            if selectedItem == item.title{
                item.isSelected = true
            }else{
                item.isSelected = false
            }
        }
        lblTitle.text = selectedItem != nil ? selectedItem : "Dashboard"
        
        self.menuCollection.delegate = self
        self.menuCollection.dataSource = self
        self.menuCollection.reloadData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.alpha = 1
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }



}
extension MenuViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMenuItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.menuCollection.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.configureCell(arrMenuItems[indexPath.row])
        cornerRadius(viewName: cell.cell_view, radius: 12)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 75, height: 75)
        
//        return CGSize(width: (UIScreen.main.bounds.width) / 4, height: 75)
    }
    
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let _ = self.mDelegate{
//            self.mDelegate?.menuOptionSelected(arrMenuItems[indexPath.row].title)
        self.dismiss(animated: true) {
            if let _ = self.parentVC{

                let title = self.arrMenuItems[indexPath.item].title
                var viewContr = UIViewController()
                var isTabVC:Bool = false
                if title == "Statistics"{
                    viewContr =  self.storyboard?.instantiateViewController(withIdentifier: "StatisticsVC") as! StatisticsVC
                    
                }
                else if title == "Plans"{
                    viewContr =  self.storyboard?.instantiateViewController(withIdentifier: "PlansVC") as! PlansVC
                    
                }
                else if title == "Dashboard"{
                    let imageDataDict:[String: Int] = ["tab": 2]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Orders"{
                    let imageDataDict:[String: Int] = ["tab": 0]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)


                }else if title == "Products"{
                    let imageDataDict:[String: Int] = ["tab": 1]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Categories"{
                    let imageDataDict:[String: Int] = ["tab": 3]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }else if title == "Settings"{
                    let imageDataDict:[String: Int] = ["tab": 4]
                    isTabVC = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tab_notification"), object: nil, userInfo: imageDataDict)

                }
                if !isTabVC{
                    self.parentVC.navigationController?.pushViewController(viewContr, animated: true)
                }
            }
        }
    }

}
