//
//  StatisticsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 30/03/22.
//

import UIKit
class UrlCell :UITableViewCell
{
    
    @IBOutlet weak var lbl_count: UILabel!
}
class StatisticsVC: UIViewController {
    
    @IBOutlet weak var height_tableview: NSLayoutConstraint!
    @IBOutlet weak var Tanleview_UrlList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Tanleview_UrlList.delegate = self
        self.Tanleview_UrlList.dataSource = self
        self.Tanleview_UrlList.reloadData()
        height_tableview.constant = 5 * 80
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
//MARK: Tableview Methods
extension StatisticsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tanleview_UrlList.dequeueReusableCell(withIdentifier: "UrlCell") as! UrlCell
        //        setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
        cell.lbl_count.text = "\(indexPath.row + 1)"
        cornerRadius(viewName: cell.lbl_count, radius: cell.lbl_count.frame.height / 2)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let ViewAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        ViewAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_eye"),
            title: "Show")
        ViewAction.backgroundColor = UIColor.init(named: "Green_Color")
        
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,ViewAction])
    }
    
    
}
