//
//  LoginVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var btn_Login: UIButton!
    @IBOutlet weak var View_Password: UIView!
    @IBOutlet weak var View_Email: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorder(viewName: self.View_Email, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_Email.frame.height / 2)
        setBorder(viewName: self.View_Password, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: self.View_Password.frame.height / 2)
        cornerRadius(viewName: self.btn_Login, radius: self.btn_Login.frame.height / 2)
       
    }
    

}
