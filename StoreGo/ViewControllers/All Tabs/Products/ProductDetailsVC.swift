//
//  ProductDetailsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 29/03/22.
//

import UIKit

class RattingsListCell : UITableViewCell
{
    
}

class ProductDetailsVC: UIViewController
{
    
    @IBOutlet weak var Height_View: NSLayoutConstraint!
    @IBOutlet weak var Tableview_RattingsList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_RattingsList.delegate = self
        self.Tableview_RattingsList.dataSource = self
        self.Tableview_RattingsList.reloadData()
        self.Height_View.constant = (10 * 175) + 51
        
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: Tableview Methods
extension ProductDetailsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_RattingsList.dequeueReusableCell(withIdentifier: "RattingsListCell") as! RattingsListCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
