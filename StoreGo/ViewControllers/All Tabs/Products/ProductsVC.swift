//
//  ProductsVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit

class ProductGridCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
}

class ProductCell : UITableViewCell
{
    @IBOutlet weak var cell_view: UIView!
}

class ProductsVC: UIViewController {
    
    @IBOutlet weak var Collectionview_productsList: UICollectionView!
    @IBOutlet weak var Tableview_ProductsList: UITableView!
    @IBOutlet var Gridview: UIView!
    @IBOutlet var ListView: UIView!
    @IBOutlet weak var Main_View: UIView!
    @IBOutlet weak var view_addProducts: UIView!
    @IBOutlet weak var btn_Grid: UIButton!
    var isview = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ListView.removeFromSuperview()
        addViewDynamically(subview: Gridview)
        
        cornerRadius(viewName: self.view_addProducts, radius: self.view_addProducts.frame.height / 2)
        self.Tableview_ProductsList.delegate = self
        self.Tableview_ProductsList.dataSource = self
        self.Tableview_ProductsList.reloadData()
        
        self.Collectionview_productsList.delegate = self
        self.Collectionview_productsList.dataSource = self
        self.Collectionview_productsList.reloadData()
        
    }
    @IBAction func btnTap_Grid(_ sender: UIButton) {
        if self.isview == "grid" {
            self.ListView.removeFromSuperview()
            addViewDynamically(subview: self.Gridview)
            self.btn_Grid.setImage(UIImage.init(named: "ic_gridview"), for: .normal)
            self.isview = "list"
        }
        else {
            self.Gridview.removeFromSuperview()
            addViewDynamically(subview: self.ListView)
            self.btn_Grid.setImage(UIImage.init(named: "ic_listview"), for: .normal)
            self.isview = "grid"
        }
    }
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Products"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnTap_Addproducts(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProductsVC") as! AddProductsVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    func addViewDynamically(subview : UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false;
        self.Main_View.addSubview(subview)
        self.Main_View.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.Main_View, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
        self.Main_View.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.Main_View, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
        self.Main_View.addConstraint(NSLayoutConstraint(item: subview, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.Main_View, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))
        self.Main_View.addConstraint(NSLayoutConstraint(item: subview
                                                        , attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.Main_View, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
        self.Main_View.layoutIfNeeded()
    }
    
    
}
//MARK: Tableview Methods
extension ProductsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_ProductsList.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
        setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 16)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let ViewAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        ViewAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_eye"),
            title: "Show")
        ViewAction.backgroundColor = UIColor.init(named: "Green_Color")
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_edit"),
            title: "Change")
        EditAction.backgroundColor = UIColor.init(named: "App_Bg_Color")
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction,ViewAction])
    }
    
    
}

//MARK: Collectionview methods
extension ProductsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.Collectionview_productsList.dequeueReusableCell(withReuseIdentifier: "ProductGridCell", for: indexPath) as! ProductGridCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 48) / 2, height: 250)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
