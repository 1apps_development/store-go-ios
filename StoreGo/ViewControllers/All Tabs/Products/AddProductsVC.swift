//
//  AddProductsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 23/03/22.
//

import UIKit

class AddProductsVC: UIViewController {
    
    @IBOutlet weak var btn_Create: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        cornerRadius(viewName: self.btn_Create, radius: self.btn_Create.frame.height / 2)
    }
    @IBAction func btnTap_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
