//
//  OrdersVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit

class OrdersVC: UIViewController {
    
    @IBOutlet weak var Tableview_RecentOrderList: UITableView!
    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var view_addNeworder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBorder(viewName: self.view_search, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.view_search.frame.height / 2)
        cornerRadius(viewName: self.view_addNeworder, radius:self.view_addNeworder.frame.height / 2)
        self.Tableview_RecentOrderList.delegate = self
        self.Tableview_RecentOrderList.dataSource = self
        self.Tableview_RecentOrderList.reloadData()
    }
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Orders"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func btnTap_Addorder(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOrderVC") as! AddNewOrderVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func btnTap_Search(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchOrderVC") as! SearchOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK: Tableview Methods
extension OrdersVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_RecentOrderList.dequeueReusableCell(withIdentifier: "RecentOrderCell") as! RecentOrderCell
        setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrdersDetailsVC") as! OrdersDetailsVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let ViewAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        ViewAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_eye"),
            title: "Show")
        ViewAction.backgroundColor = UIColor.init(named: "Green_Color")
        // ViewAction.cornerRadius = 5.0
        
        //        cornerRadius(viewName: ViewAction, radius: 8.0)
        
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        //        cornerRadius(viewName: deleteAction, radius: 8.0)
        
        return UISwipeActionsConfiguration(actions: [deleteAction,ViewAction])
    }
    
    
}
