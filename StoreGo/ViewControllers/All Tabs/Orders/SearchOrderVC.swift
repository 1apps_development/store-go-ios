//
//  SearchOrderVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 04/04/22.
//

import UIKit

class SearchOrderVC: UIViewController {
    
    @IBOutlet weak var Tableview_RecentOrderList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_RecentOrderList.delegate = self
        self.Tableview_RecentOrderList.dataSource = self
        self.Tableview_RecentOrderList.reloadData()
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: Tableview Methods
extension SearchOrderVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.Tableview_RecentOrderList.dequeueReusableCell(withIdentifier: "RecentOrderCell") as! RecentOrderCell
        setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrdersDetailsVC") as! OrdersDetailsVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let ViewAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        ViewAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_eye"),
            title: "Show")
        ViewAction.backgroundColor = UIColor.init(named: "Green_Color")
        
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,ViewAction])
    }
    
}
