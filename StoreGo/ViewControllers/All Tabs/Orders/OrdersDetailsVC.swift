//
//  OrdersDetailsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 30/03/22.
//

import UIKit
class orderListCell : UITableViewCell
{
    
}

class OrdersDetailsVC: UIViewController {

    @IBOutlet weak var Tableview_Orderslist: UITableView!
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_Orderslist.delegate = self
        self.Tableview_Orderslist.dataSource = self
        self.Tableview_Orderslist.reloadData()
        self.Height_Tableview.constant = 10 * 40

       
    }
    
    
    @IBAction func btnTap_close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK: Tableview Methods
extension OrdersDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_Orderslist.dequeueReusableCell(withIdentifier: "orderListCell") as! orderListCell
//        setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 16)
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    
}
