//
//  SearchCategoriesVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 04/04/22.
//

import UIKit

class SearchCategoriesVC: UIViewController {

    @IBOutlet weak var TableView_CategoryList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.TableView_CategoryList.delegate = self
        self.TableView_CategoryList.dataSource = self
        self.TableView_CategoryList.reloadData()
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
//MARK: Tableview Methods
extension SearchCategoriesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_CategoryList.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_edit"),
            title: "Change")
        EditAction.backgroundColor = UIColor.init(named: "App_Bg_Color")
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction])
    }
    
    
}
