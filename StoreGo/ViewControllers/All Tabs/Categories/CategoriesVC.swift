//
//  CategoriesVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit

class CategoriesCell : UITableViewCell
{
    
}
class CategoriesVC: UIViewController {
    
    @IBOutlet weak var view_Search: UIView!
    @IBOutlet weak var View_Categories: UIView!
    
    @IBOutlet weak var TableView_CategoryList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TableView_CategoryList.delegate = self
        self.TableView_CategoryList.dataSource = self
        self.TableView_CategoryList.reloadData()
        cornerRadius(viewName: self.View_Categories, radius: self.View_Categories.frame.height / 2)
        setBorder(viewName: self.view_Search, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.view_Search.frame.height / 2)
        
    }
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Categories"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func btnTap_AddCategories(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddCategoriesVC") as! AddCategoriesVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func btnTap_Search(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoriesVC") as! SearchCategoriesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK: Tableview Methods
extension CategoriesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_CategoryList.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_edit"),
            title: "Change")
        EditAction.backgroundColor = UIColor.init(named: "App_Bg_Color")
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction])
    }
    
    
}

extension UISwipeActionsConfiguration {
    
    public static func makeTitledImage(
        image: UIImage?,
        title: String,
        textColor: UIColor = .white,
        font: UIFont = UIFont.init(name: "Outfit Medium", size: 12)!,
        size: CGSize = .init(width: 50, height: 50)
    ) -> UIImage? {
        
        /// Create attributed string attachment with image
        let attachment = NSTextAttachment()
        attachment.image = image
        let imageString = NSAttributedString(attachment: attachment)
        
        /// Create attributed string with title
        let text = NSAttributedString(
            string: "\n\(title)",
            attributes: [
                .foregroundColor: textColor,
                .font: font
            ]
        )
        
        /// Merge two attributed strings
        let mergedText = NSMutableAttributedString()
        mergedText.append(imageString)
        mergedText.append(text)
        
        /// Create label and append that merged attributed string
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        label.textAlignment = .center
        label.numberOfLines = 2
        label.attributedText = mergedText
        
        /// Create image from that label
        let renderer = UIGraphicsImageRenderer(bounds: label.bounds)
        let image = renderer.image { rendererContext in
            label.layer.render(in: rendererContext.cgContext)
        }
        
        /// Convert it to UIImage and return
        if let cgImage = image.cgImage {
            return UIImage(cgImage: cgImage, scale: UIScreen.main.scale, orientation: .up)
        }
        
        return nil
    }
}
