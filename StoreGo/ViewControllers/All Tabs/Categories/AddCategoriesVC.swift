//
//  AddCategoriesVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 22/03/22.
//

import UIKit

class AddCategoriesVC: UIViewController {

    @IBOutlet weak var view_Name: UIView!
    @IBOutlet weak var btn_Create: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBorder(viewName: self.view_Name, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.view_Name.frame.height / 2)
        cornerRadius(viewName: self.btn_Create, radius: self.btn_Create.frame.height / 2)
        
    }
    @IBAction func btnTap_Close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
