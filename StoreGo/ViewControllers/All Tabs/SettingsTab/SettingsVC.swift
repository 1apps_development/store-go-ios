//
//  SettingsVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit

class SettingsCell : UITableViewCell
{
    @IBOutlet weak var img_images: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
    func configureCellWith(_ settins: SettingItem){
        lbl_title.text = settins.title
        img_images.image = UIImage.init(named: settins.image)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
class SettingsVC: UIViewController {

    @IBOutlet weak var TableView_Settings: UITableView!
    var arrSettings = [SettingItem(title: "My Account", image: "ic_myprofile"),
                       SettingItem(title: "Store Settings", image: "ic_storeSetting"),
                       SettingItem(title: "Store Theme Settings", image: "ic_storetheme"),
                       SettingItem(title: "Store Payment", image: "ic_storepayment"),
                       SettingItem(title: "Store Email Setting", image: "ic_storeemail"),
                       SettingItem(title: "Whatsapp Message Setting", image: "ic_storemsg")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableView_Settings.delegate = self
        self.TableView_Settings.dataSource = self
        self.TableView_Settings.reloadData()
        
        
    }
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Settings"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
    func openViewControllerWithName(names: String){
        let vc = self.storyboard!.instantiateViewController(withIdentifier: names)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: Tableview Methods
extension SettingsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TableView_Settings.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        cell.configureCellWith(arrSettings[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            self.openViewControllerWithName(names: "MyAccountVC")
        }
        else if indexPath.row == 1
        {
            self.openViewControllerWithName(names: "StoreSettingsVC")
        }
        else if indexPath.row == 2
        {
            self.openViewControllerWithName(names: "StoreThemeSettingsVC")
        }
        else if indexPath.row == 3
        {
            self.openViewControllerWithName(names: "StorePaymentVC")
        }
        else if indexPath.row == 4
        {
            self.openViewControllerWithName(names: "StoreEmailSettingVC")
        }
        else if indexPath.row == 5
        {
            self.openViewControllerWithName(names: "WhatsappMassageSettingVC")
        }
    }
    
}
