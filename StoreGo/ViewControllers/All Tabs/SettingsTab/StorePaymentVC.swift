//
//  StorePaymentVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 04/04/22.
//

import UIKit
class PaymentCell : UITableViewCell
{
    @IBOutlet weak var lbl_title: UILabel!
    
}
class StorePaymentVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_PaymentList: UITableView!
    var PaymentArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.PaymentArray = ["COD","Telegram","Whatsapp","Bank Transfer","Stripe","PayPal","Paystack","Flutterwave","Razorpay","Paytm","Mercado Pago","Mollie","Skrill","CoinGate","PaymentWall"]
        self.Tableview_PaymentList.dataSource = self
        self.Tableview_PaymentList.delegate = self
        self.Height_Tableview.constant = CGFloat(self.PaymentArray.count * 60)
       
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: Tableview Methods
extension StorePaymentVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.PaymentArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.Tableview_PaymentList.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        cell.lbl_title.text = self.PaymentArray[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
}
