//
//  StoreSettingsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 22/03/22.
//

import UIKit

class StoreSettingsVC: UIViewController {

    
    @IBOutlet weak var view_Email: UIView!
    @IBOutlet weak var view_storeName: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBorder(viewName: self.view_Email, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.view_Email.frame.height / 2)
        setBorder(viewName: self.view_storeName, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.view_storeName.frame.height / 2)
        
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}



