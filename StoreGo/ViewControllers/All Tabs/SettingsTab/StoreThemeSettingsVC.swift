//
//  StoreThemeSettingsVC.swift
//  StoreGo
//
//  Created by DREAMWORLD on 04/04/22.
//

import UIKit

class TemplateCell : UICollectionViewCell
{
    
}
class StoreThemeSettingsVC: UIViewController {

    @IBOutlet weak var Height_Collectionview: NSLayoutConstraint!
    @IBOutlet weak var collectionview_TempletList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionview_TempletList.delegate = self
        self.collectionview_TempletList.dataSource = self
        
      self.Height_Collectionview.constant = 2 * 260
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
//MARK: Collectionview methods
extension StoreThemeSettingsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionview_TempletList.dequeueReusableCell(withReuseIdentifier: "TemplateCell", for: indexPath) as! TemplateCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 48) / 2, height: 250)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    
}
