//
//  DashboardVC.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit
import JJFloatingActionButton
class StatisticsListCell : UITableViewCell
{
    
    @IBOutlet weak var cell_view: UIView!
}
class StatisticsListCell2 : UITableViewCell
{
    
    @IBOutlet weak var cell_view: UIView!
}
class TopProductsCell : UITableViewCell
{
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var cell_view: UIView!
}
class RecentOrderCell : UITableViewCell
{
    @IBOutlet weak var cell_view: UIView!
}

class DashboardVC: UIViewController {

    fileprivate let actionButton = JJFloatingActionButton()
    @IBOutlet weak var viewCircle: UIView!
    
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_Mystore: UIImageView!
    @IBOutlet weak var View_MyStore: UIView!
    
    @IBOutlet weak var view_morestatistics: UIView!
    @IBOutlet weak var view_Gotoproducts: UIView!
    @IBOutlet weak var view_Gotoorders: UIView!
    
    @IBOutlet weak var btn_showmoreorders: UIButton!
    @IBOutlet weak var view_quickAdd: UIView!
    
    @IBOutlet weak var Tableview_StatisticsList: UITableView!
    
    @IBOutlet weak var height_TableviewstatisticsList: NSLayoutConstraint!
    
    @IBOutlet weak var Tableview_TopProductsList: UITableView!
    @IBOutlet weak var height_TableviewTopProductsList: NSLayoutConstraint!
    
    
    @IBOutlet weak var Tableview_RecentOrderList: UITableView!
    @IBOutlet weak var height_TableviewRecentOrderList: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBorder(viewName: self.View_MyStore, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: self.View_MyStore.frame.height / 2)
        cornerRadius(viewName: self.img_user, radius: self.img_user.frame.height / 2)
        cornerRadius(viewName: self.img_Mystore, radius: self.img_Mystore.frame.height / 2)
        
        cornerRadius(viewName: self.view_quickAdd, radius: self.view_quickAdd.frame.height / 2)
        
        cornerRadius(viewName: self.view_morestatistics, radius: self.view_morestatistics.frame.height / 2)
        cornerRadius(viewName: self.view_Gotoorders, radius: self.view_Gotoorders.frame.height / 2)
        cornerRadius(viewName: self.view_Gotoproducts, radius: self.view_Gotoproducts.frame.height / 2)
        cornerRadius(viewName: self.btn_showmoreorders, radius: self.btn_showmoreorders.frame.height / 2)
        
        
        self.Tableview_StatisticsList.delegate = self
        self.Tableview_StatisticsList.dataSource = self
        self.Tableview_StatisticsList.reloadData()
        self.height_TableviewstatisticsList.constant = 4 * 80
        
        self.Tableview_TopProductsList.delegate = self
        self.Tableview_TopProductsList.dataSource = self
        self.Tableview_TopProductsList.reloadData()
        self.height_TableviewTopProductsList.constant = 5 * 90
        
        
        self.Tableview_RecentOrderList.delegate = self
        self.Tableview_RecentOrderList.dataSource = self
        self.Tableview_RecentOrderList.reloadData()
        self.height_TableviewRecentOrderList.constant = 5 * 100
        
        self.FloatingActionButton()
    }
    func FloatingActionButton()
    {
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        actionButton.delegate = self
        actionButton.buttonImageColor = hexStringToUIColor(hex: "172C4E")
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonImageSize = CGSize(width: 16, height: 16)
        actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: 80)
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        actionButton.buttonAnimationConfiguration.opening.duration = 0.8
        actionButton.buttonAnimationConfiguration.closing.duration = 0.6
        
        for item in actionButton.items{
            item.imageView.tintColor = hexStringToUIColor(hex: "172C4E")
        }
        
        if #available(iOS 13.0, *) {
            actionButton.buttonImage = UIImage.init(named: "ic_plus")?.withTintColor(hexStringToUIColor(hex: "172C4E"))
        } else {
            // Fallback on earlier versions
        }
        
        let imageProj = UIImage.init(named: "ic_menuhome")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageInvo = UIImage.init(named: "ic_menucart")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageTasks = UIImage.init(named: "ic_menuList")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        
        actionButton.addItem(image: imageProj) { item in
//            Helper.showAlert(for: item)
        }

        actionButton.addItem(image: imageInvo) { item in
//            Helper.showAlert(for: item)
        }

        actionButton.addItem(image: imageTasks) { item in
//            Helper.showAlert(for: item)
        }
        
//        actionButton.display(inViewController: self)
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant:-18).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        viewCircle.center = actionButton.center
        viewCircle.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
    @IBAction func btnTap_Menu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        vc.parentVC = self
        vc.selectedItem = "Dashboard"
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: false, completion: nil)
    }
}

extension DashboardVC : JJFloatingActionButtonDelegate
{
    func floatingActionButtonWillOpen(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = .white
        viewCircle.isHidden = false
    }
    func floatingActionButtonWillClose(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        viewCircle.isHidden = true
    }
}
//MARK: Tableview Methods
extension DashboardVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.Tableview_StatisticsList
        {
            return 4
        }
        else if tableView == self.Tableview_TopProductsList
        {
            return 5
        }
        else{
            return 5
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.Tableview_StatisticsList
        {
            return 80
        }
        else if tableView == self.Tableview_TopProductsList
        {
            return 90
        }
        else{
            return 100
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.Tableview_StatisticsList
        {
            if indexPath.row == 0
            {
                let cell = self.Tableview_StatisticsList.dequeueReusableCell(withIdentifier: "StatisticsListCell") as! StatisticsListCell
                
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
                return cell
            }
            else
            {
                let cell = self.Tableview_StatisticsList.dequeueReusableCell(withIdentifier: "StatisticsListCell2") as! StatisticsListCell2
                
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
                return cell
            }
        }
        else if tableView == self.Tableview_TopProductsList
        {
            
                let cell = self.Tableview_TopProductsList.dequeueReusableCell(withIdentifier: "TopProductsCell") as! TopProductsCell
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
                cell.lbl_count.text = "\(indexPath.row + 1)"
            cornerRadius(viewName: cell.lbl_count, radius: cell.lbl_count.frame.height / 2)
                return cell
            
        }
        else{
           
                let cell = self.Tableview_RecentOrderList.dequeueReusableCell(withIdentifier: "RecentOrderCell") as! RecentOrderCell
                
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.init(named: "Text_Color")!.cgColor, cornerRadius: 12)
                return cell
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}
